#!/bin/bash
sudo service apache2 stop

sudo cp -f default /etc/apache2/sites-available/

sudo rm -rf /var/www

sudo cp -rf www /var/

sudo chmod 777 /var/www/uploads

sudo service apache2 start
