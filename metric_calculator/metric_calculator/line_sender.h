#ifndef LINE_SENDER_H_INCLUDED
#define LINE_SENDER_H_INCLUDED

#include <istream>
#include "metric.h"
#include <vector>

class line_sender
{
public:
    void add_metric(metric *target_metric);
    void extract_lines(std::istream & stream);
    int calculate_star_rating();
private:
    std::vector<metric *> stored_metric;
};

#endif
