#ifndef METRIC_H_INCLUDED
#define METRIC_H_INCLUDED

#include <string>

class metric
{
public:
    virtual void parse_line(const std::string & line) = 0;
    virtual int metric_result() = 0;
};

#endif
