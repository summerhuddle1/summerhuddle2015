#ifndef FUNCTION_LINES_INCLUDED
#define FUNCTION_LINES_INCLUDED

#include "metric.h"
#include <vector>


struct function_metric_type
{
    enum value {MAX , AVERAGE};
};


class function_lines : public metric
{
public:
    function_lines(function_metric_type::value function_type); //This is the constructor

    virtual void parse_line(const std::string & line);
    float average_lines_per_function();
    int metric_result();
private:
    int number_of_lines;
    int depth_count;
    int number_of_functions;
    std::vector <int> line_count;
    function_metric_type::value function_type;


};


#endif
