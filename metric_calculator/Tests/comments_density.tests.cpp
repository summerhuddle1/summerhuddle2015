#include "comments_density.h"
#include "gtest/gtest.h"

using namespace testing;
using namespace std;


TEST(comments_density, uncommented_lines_gives_0)
{
    comments_density hello_comments;
    hello_comments.parse_line("Hello World");
    hello_comments.parse_line("fox ");
    hello_comments.parse_line("Hello/ World");

    ASSERT_EQ(0, hello_comments.metric_result());
}

TEST(comments_density, one_line_consisting_of_half_the_characters_in_a_comment_gives_1)
{
    comments_density hello_comments;
    hello_comments.parse_line("not a comment//Hello World");

    ASSERT_EQ(0, hello_comments.metric_result());
}

TEST(comments_density, multiple_lines_with_less_than_10_percent_commented_gives_1)
{
    comments_density hello_comments;
    hello_comments.parse_line("Hello World");
    hello_comments.parse_line("fox dog cat //d");
    hello_comments.parse_line("Hello/ World");

    ASSERT_EQ(1, hello_comments.metric_result());
}

TEST(comments_density, fully_commented_out_line_gives_0)
{
    comments_density hello_comments;
    hello_comments.parse_line("//Hello //World");

    ASSERT_EQ(0, hello_comments.metric_result());
}
