#include "function_lines.h"
#include <gtest/gtest.h>

using namespace ::testing;


TEST(function_lines_tests,average_number_of_lines_per_function)
{
    function_lines line_metric(function_metric_type::AVERAGE);

    line_metric.parse_line("#include function_lines.h");
    line_metric.parse_line("#include <string>");
    line_metric.parse_line("#include <vector>");
    line_metric.parse_line("using namespace std;");
    line_metric.parse_line("function_lines::function_lines() : line_count(1, 0)");
    line_metric.parse_line("{");
    line_metric.parse_line("number_of_lines=1;");
    line_metric.parse_line("depth_count=0;");
    line_metric.parse_line("number_of_functions=0;");
    line_metric.parse_line("}");
    line_metric.parse_line("");
    line_metric.parse_line("void function_lines::parse_line(const std::string &line )");
    line_metric.parse_line("{");
    line_metric.parse_line("    for(size_t i=0; i<line.length();i++)");
    line_metric.parse_line("    {");
    line_metric.parse_line("        if (line[i]=='{')");
    line_metric.parse_line("            depth_count++;");
    line_metric.parse_line("");
    line_metric.parse_line("        if (line[i]=='}')");
    line_metric.parse_line("        {");
    line_metric.parse_line("            depth_count--;");
    line_metric.parse_line("");
    line_metric.parse_line("            if (depth_count==0)");
    line_metric.parse_line("            {");
    line_metric.parse_line("            line_count[number_of_functions]=number_of_lines;");
    line_metric.parse_line("                number_of_functions++;");
    line_metric.parse_line("                line_count.push_back (0);");
    line_metric.parse_line("                number_of_lines=1;");
    line_metric.parse_line("            }");
    line_metric.parse_line("        }");
    line_metric.parse_line("    }");
    line_metric.parse_line("    if (depth_count>0)");
    line_metric.parse_line("        number_of_lines++;");
    line_metric.parse_line("}");
    line_metric.parse_line("");
    line_metric.parse_line("float function_lines::average_lines_per_function()");
    line_metric.parse_line("{");
    line_metric.parse_line("    float average=0;");
    line_metric.parse_line("    for (int i=0; i<number_of_functions;i++)");
    line_metric.parse_line("    {");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("    }");
    line_metric.parse_line("");
    line_metric.parse_line("    return average/number_of_functions;");
    line_metric.parse_line("}");
    line_metric.parse_line("");


    ASSERT_NEAR(12, line_metric.average_lines_per_function(),0.01);
}

TEST(function_lines_tests,file_with_no_functions_returns_0)
{
    function_lines line_metric(function_metric_type::AVERAGE);
    line_metric.parse_line("#include function_lines.h");
    ASSERT_EQ(0, line_metric.average_lines_per_function());
}

TEST(function_lines_tests,returns_metric_result_of_1_with_0_lines)
{
    function_lines line_metric(function_metric_type::AVERAGE);
    line_metric.parse_line("#include function_lines.h");
    ASSERT_EQ(1, line_metric.metric_result());
}

TEST(function_lines_tests,returns_metric_result_of_0_with_lots_of_lines)
{
    function_lines line_metric(function_metric_type::AVERAGE);
    line_metric.parse_line("#include function_lines.h");
    line_metric.parse_line("{");
    line_metric.parse_line("    float average=0;");
    line_metric.parse_line("    for (int i=0; i<number_of_functions;i++)");
    line_metric.parse_line("    {");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("    }");
    line_metric.parse_line("");
    line_metric.parse_line("    return average/number_of_functions;");
    line_metric.parse_line("}");
    ASSERT_EQ(0, line_metric.metric_result());
}

TEST(function_lines_tests,number_of_lines_per_function)
{
    function_lines line_metric(function_metric_type::MAX);

    line_metric.parse_line("#include function_lines.h");
    line_metric.parse_line("#include <string>");
    line_metric.parse_line("#include <vector>");
    line_metric.parse_line("using namespace std;");
    line_metric.parse_line("function_lines::function_lines() : line_count(1, 0)");
    line_metric.parse_line("{");
    line_metric.parse_line("number_of_lines=1;");
    line_metric.parse_line("depth_count=0;");
    line_metric.parse_line("number_of_functions=0;");
    line_metric.parse_line("}");
    line_metric.parse_line("");
    line_metric.parse_line("void function_lines::parse_line(const std::string &line )");
    line_metric.parse_line("{");
    line_metric.parse_line("    for(size_t i=0; i<line.length();i++)");
    line_metric.parse_line("    {");
    line_metric.parse_line("        if (line[i]=='{')");
    line_metric.parse_line("            depth_count++;");
    line_metric.parse_line("");
    line_metric.parse_line("        if (line[i]=='}')");
    line_metric.parse_line("        {");
    line_metric.parse_line("            depth_count--;");
    line_metric.parse_line("");
    line_metric.parse_line("            if (depth_count==0)");
    line_metric.parse_line("            {");
    line_metric.parse_line("            line_count[number_of_functions]=number_of_lines;");
    line_metric.parse_line("                number_of_functions++;");
    line_metric.parse_line("                line_count.push_back (0);");
    line_metric.parse_line("                number_of_lines=1;");
    line_metric.parse_line("            }");
    line_metric.parse_line("        }");
    line_metric.parse_line("    }");
    line_metric.parse_line("    if (depth_count>0)");
    line_metric.parse_line("        number_of_lines++;");
    line_metric.parse_line("}");
    line_metric.parse_line("");
    line_metric.parse_line("float function_lines::average_lines_per_function()");
    line_metric.parse_line("{");
    line_metric.parse_line("    float average=0;");
    line_metric.parse_line("    for (int i=0; i<number_of_functions;i++)");
    line_metric.parse_line("    {");
    line_metric.parse_line("        average+=line_count[i];");
    line_metric.parse_line("    }");
    line_metric.parse_line("");
    line_metric.parse_line("    return average/number_of_functions;");
    line_metric.parse_line("}");
    line_metric.parse_line("");


    ASSERT_EQ(1, line_metric.metric_result());
}
